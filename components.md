# holoComponents Resources 

## holoCryption
  - framagit : <https://framagit.org/holoverse/holoarchi/holocryption>
  - framapad : <https://annuel2.framapad.org/p/holoCryption>


## holoAtoms
  - framagit : https://framagit.org/holosphere/holocore/holoatoms
  - framapad : https://annuel2.framapad.org/p/holoAtoms


## Holopad (ether pad/ framapad)
  - gitlab : https://gitlab.com/holoverse4/holotools/holopad


##  HoloGit 
  - gitlab : https://gitlab.com/kinLabs/holoGIT
  - framapad : https://annuel2.framapad.org/p/holoGIT


## holoGate
  - framagit : https://framagit.org/holosphere/hologate
  - framapad : https://annuel2.framapad.org/p/holoGate
  - setup (install IPFS/GIT/nextcoud on mobile)  
  - Holoservers (list) (nextcloud, civetweb, netlify's JAMstack, ... heroku etc.)
  - HoloDNS and holoLNS (Domain and Location Naming Services)
  - Hologateway ( like https://ipfs.blockringtm.ml ) (ethical gateways)


## holoCode (IRP) (lists; algo, hashes etc.)
  - gitlab: https://gitlab.com/holoarchi/holocode
  - framapad: https://annuel2.framapad.org/p/holoCode


## HoloKit (IPFS mobile/iPad/computer): Package  management i.e. installation of holoSphere to have graceful installation)
  - gitlab : https://gitlab.com/holokit/holokit.gitlab.io
  - framapad: https://annuel2.framapad.org/p/holoKIT


