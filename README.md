## holoIndexes


holoIndexes is the entry point to find all ressources avalaible in the holoVerse

we hve the url for the framapad, framagit, gitlab repositories, as well
as other online assets.


## holoPads :

- [holoCryption](https://annuel2.framapad.org/p/holoCryption)
- [holoAtoms](https://annuel2.framapad.org/p/holoAtoms)
- [Holopad](https://annuel2.framapad.org/p/holoPad) (ether pad/ framapad)
- [HoloGit](https://annuel2.framapad.org/p/holoGIT)
- [holoGate](https://annuel2.framapad.org/p/holoGate)
- [holoCode](https://annuel2.framapad.org/p/holoCode) (IRP) (lists; algo, hashes etc.)
- [HoloKit](https://annuel2.framapad.org/p/holoKIT) (IPFS mobile/iPad/computer): Package  management i.e. installation of

## holoGit :

- [holoCryption](https://framagit.org/holoverse/holoarchi/holocryption)
- [holoAtoms](https://framagit.org/holosphere/holocore/holoatoms)
- [Holopad](https://gitlab.com/holoverse4/holotools/holopad)
- [HoloGit](https://gitlab.com/kinLabs/holoGIT)
- [holoGate](https://framagit.org/holosphere/hologate)
- [holoCode](https://gitlab.com/holoarchi/holocode)
- [HoloKit](https://gitlab.com/holokit/holokit.gitlab.io)


## Sharded indexes for holoResolver


Indexes are sharded to allow a quick load and distribution w/i the *KIN*etwork

indexes are collected on a use basis,
i.e. all links "clicked" from a search via holoSearch & holoCompass



<!--
org: https://github.com/kinLabs
repo: https://gitlab.com/xn--1g8h/overview/-/commits/master

### activities:

- https://framagit.org/dashboard/activity :
  feed: https://framagit.org/dashboard/projects.atom?feed_token=vzHih8Qx1L5kBZ1wSPDm
- https://gihub.com/dashboard/activity
  feed: https://gitlab.com/dashboard/projects.atom?feed_token=GyELSuZa6Y4yDkRrjmCz



### badges:

- http://img.shields.io/... (version, license etc.)
- https://img.shields.io/maintenance/yes/2020.svg
- http://readthedocs.org/projects/holotools/badge
- https://git.framasoft.org/spalax/holotools/badges/master/coverage.svg
- https://framagit.org/spalax/evariste/builds
-->


