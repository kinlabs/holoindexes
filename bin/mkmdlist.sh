#

export PATH=../bin:$PATH
date=$(date)
mdlf=$HOME/my/etc/mdlist.lof
mv $mdlf ${mdlf}~
perl -S uniquify.pl $mdlf~ > $mdlf
echo "# \$Date: $date \$" >> $mdlf

echo "# iggy's:" >> $mdlf
find /home/iggy -name "*.md" -print >> $mdlf 2>/dev/null
echo "# HDD:" >> $mdlf
find /mnt/HDD -name "*.md" -print >>$mdlf

echo "HOME:"
echo "# $HOME:" >> $mdlf
find $HOME -name "*.md" -print | tee -a $mdlf | sed 's/^/  /'
#find $HOME -name "*.md" -exec xlink.pl {} \; -print | tee -a $mdlf

echo list: $mdlf

## file 32 of them !
perl -S summarize.pl $mdlf | perl -S index.pl

