#/usr/bin/env perl

# display the 12 top, 8 random and 12 bottom lines

my $i = 0;
my $p = 0;
my @buf = ();

while (<>) {
 print if ($. <= 12);
 print "# ...\n" if ($. == 12);
 if ($p < 12 && rand()<0.5 && $. > 12) {
  $p++;
  print;
 }
 $buf[$i++%12] = $_;
}
print "# ...\n";
for (0 .. 11) {
 print $buf[$i++%12];
}

exit $?;
1; # $Source: /my/perl/scripts/summarize.pl$
 
 

