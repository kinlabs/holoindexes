#!/usr/bin/env perl

while (<>) {
  chomp;
  last if /^:q/;
  printf "line: %s\n",$_;
  my $md5 = &hashr('MD5',1,$_);
  my $sha1 = &hashr('SHA1',1,$_);
  my $sha2 = &hashr('SHA256',1,$_);
  printf "md5: %s\n",uc unpack'H*',$md5;
  printf "md5z: z%s\n",&encode_base58($md5);

  printf "sha1: %s\n",unpack'H*',$sha1;
  printf "sha1z: z%s\n",&encode_base58($sha1);
  printf "sha1m: m%s\n",&encode_base64m($sha1);

  printf "sha2: %s\n",&encode_base64u($sha2);
  printf "sha2f: f%s\n",unpack'H*',$sha2;
  printf "sha2z: z%s\n",&encode_base58($sha2);
  printf "qm1: z%s\n",&encode_base58("\x01\x55\x11\x14".$sha1);
  printf "qm: z%s\n",&encode_base58("\x01\x55\x12\x20".$sha2);
  last if /^(?:exit|bye|quit)/;
}

# -----------------------------------------------------
sub hashr {
   my $alg = shift;
   my $rnd = shift; # number of round to run ...
   my $tmp = join('',@_);
   use Crypt::Digest qw();
   my $msg = Crypt::Digest->new($alg) or die $!;
   for (1 .. $rnd) {
      $msg->add($tmp);
      $tmp = $msg->digest();
      $msg->reset;
      #printf "#%d tmp: %s\n",$_,unpack'H*',$tmp;
   }
   return $tmp
}
# -----------------------------------------------------
sub encode_base58 { # btc
  use Math::BigInt;
  use Encode::Base58::BigInt qw();
  my $bin = join'',@_;
  my $bint = Math::BigInt->from_bytes($bin);
  my $h58 = Encode::Base58::BigInt::encode_base58($bint);
  $h58 =~ tr/a-km-zA-HJ-NP-Z/A-HJ-NP-Za-km-z/;
  return $h58;
}
# -----------------------------------------------------
sub encode_base64m {
  use MIME::Base64 qw();
  my $m64 = MIME::Base64::encode_base64($_[0],'');
  return $m64;
}
# -----------------------------------------------------
sub encode_base64u {
  use MIME::Base64 qw();
  my $u64 = MIME::Base64::encode_base64($_[0],'');
  $u64 =~ y,+/,-_,;
  return $u64;
}
# -----------------------------------------------------
sub fname { # extract filename etc...
  my $f = shift;
  $f =~ s,\\,/,g; # *nix style !
  my $s = rindex($f,'/');
  my $fpath = '.';
  if ($s > 0) {
    $fpath = substr($f,0,$s);
  } else {
    use Cwd;
    $fpath = Cwd::getcwd();
  }
  my $file = substr($f,$s+1);
  if (-d $f) {
    return ($fpath,$file);
  } else {
  my $bname;
  my $p = rindex($file,'.');
  my $ext = lc substr($file,$p+1);
     $ext =~ s/\~$//;
  if ($p > 0) {
    $bname = substr($file,0,$p);
    $ext = lc substr($file,$p+1);
    $ext =~ s/\~$//;
  } else {
    $bname = $file;
    $ext = &get_ext($f);
  }
  $bname =~ s/\s+\(\d+\)$//; # remove (1) in names ...

  return ($fpath,$file,$bname,$ext);

  }
}
# -----------------------------------------------------
1; # $Source: /my/perl/scripts/index.pl$
