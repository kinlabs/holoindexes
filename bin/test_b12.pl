#!/usr/bin/env perl


use lib '/home/michelc/github.com/site/lib/';
use UTIL qw(encode_base12 decode_base12);


for my $i (0 .. 24) {
my $b12 = &encode_base12(pack'n',$i);
my $s = &decode_base12($b12);
printf "%-2s: b12=%2s h=%s n=%s\n",$i,$b12,unpack('H*',$s),unpack'n',$s;
}
