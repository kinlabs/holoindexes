#!/usr/bin/env perl

# this script extract the source keyword from the file content
while (<>) {
 # /!\ keywords regexp loop ... w/ lookbehind " or \n and lookahead \\
 if (m/(?<![\\\$])\$(Source):\s*([^\\\$]*?)\s*(?>!\\)?\$(?=['"\s])/) { # !~ on /\$.*$/
   print $2;
 }
}
exit $?;

1; # $Source: /my/perl/scripts/getsource.pl $
