#!/usr/bin/env perl

# this script take a list of file as input (or a du output) and
# compute the hash to build a index table
# usage
# ls -tr1 | tail -3 | perl index.pl

my $p = rindex(__FILE__,'/');
my $bindir = ($p > 0) ? substr(__FILE__,0,$p) : '.'; 
my $root = $bindir.'/../shards';
printf "root: %s\n",$root;

while (<>) {
  chomp;
  my $time = time;
  # mutable part :
  my $file = (split/\s+/,$_)[-1];
  my ($fpath,$fname,$bname,$ext);
  if (-d $file) {
    $file .= '/index.yml';
    next if ! -e $file;
  } elsif  (-e $file) {
     ($fpath,$fname,$bname,$ext) = &fname($file);
    printf "file: %s\n",$file;
    next if $file =~ m,shards/[^/]*/../,; # indexes...
    next if $file =~ m,/\.\w,; # .files
    next if $file =~ m,(?:secure|private|\.sec|\.key|/id_|/keys|\.lo?ck|/objects),; # .files
  } else {
     $content = $_;
     ($fpath,$fname,$bname,$ext) = ('/dev/stdin','content.dat','content','blob');
     printf "file: %s\n",$fpath;
  } 
  my $source = &get_source($file);
  printf "source: %s\n",$source;
  # parent
  my $parent = $fpath . '/';
  printf "bname: %s\n",$bname;
  # tokenized name
  my $md5 = &khash('MD5',lc$bname);
  my $sha1 = &khash('SHA1',lc$bname);
  printf "md5: f%s\n",unpack'H*',$md5;
  printf "sha1: f%s\n",unpack'H*',$sha1;
  printf "wiki: %s.%s\n",&wikipage($bname),$ext;

  my $sha2 = &khash('SHA256',$parent);
  printf "sha2: z%s\n",&encode_base58("\x01\x55\x12\x20".$sha2);
  
  my $mut = &khash('SHA256',$source);
  my $mut160 = substr($mut,0,20);
  printf "mut: b%s\n",&encode_base32("\x01\x55\x12\x14".$mut160);
  printf "mt160: z%s\n", &encode_base58("\x01\x55\x12\x14".$mut160);
  #printf "mt256: z%s\n", &encode_base58("\x01\x55\x12\x20".$mut);
  my $mt72 = &encode_basen($mut,72); # alphanum and -+.@$%_,~ 
  printf "mt72: %s\n", $mt72;

  # immutable part
  my $qm,$khash,$gitid;
  if (-e $file) {
    $khash = &get_khash('SHA256',$file);
    $mhash = "\x01\x55\x12\x20".$khash;
    $qm = 'z'. &encode_base58($mhash);
    ($size,$time) = (lstat($file))[7,9];
    my $ns = sprintf "blob %u\0",$size;
    $gitid = &get_khash('SHA1',$ns,$file);
  } elsif ($content =~ m/^#/) {
    $khash = &khash('SHA256',$content);
    $mhash = "\x01\x55\x12\x20".$khash;
    $qm = 'z'. &encode_base58($mhash);
    my $ns = sprintf "blob %u\0",length($content);
    $gitid = &get_khash('SHA1',$ns,$file);
  } else {
    # echo '410 gone' | ipfs add -Q -n --hash sha3-224 --cid-base base58btc
    $qm = 'z6CfPrZfRNH82i6zLdFnmdfPaxmfw3qCFvD81BFC2ifx';
    printf "qm58: %s\n",$qm;
    #$content = '410 gone';
    #$khash = &khash('SHA256',$content);
    #$mhash = &decode_base58(substr($qm,1));
    next;
  }
  printf "gitid: %s\n",unpack'H*',$gitid;
  printf "qm58: %s\n",$qm;

  my $p = $time % length($mut); # note: it uses mtim if file exists
  my $c = unpack'C',substr($mut,$p,1);
  my $tic = 256 * int($time / 256) + $c;
  printf "tic: %u\n",$tic;


  my $idxf;
  # bname:
  $idxf = &add_record($root.'/bnames',$tic,$sha1,"file.$ext",$mhash,$ext);
  printf "%s.idx: %s\n",$bname,$idxf;
  # git:
  $idxf = &add_gitobject($root.'/objects',$tic,$gitid,"$bname.$ext",$mhash,$ext);
  printf "git-blob.idx: %s\n",$idxf;

  my $label = lc $bname;
  # parent
  $idxf = &add_record($root.'/parent',$tic,$sha2,$label,$mhash,$ext);
  printf "parent.idx: %s\n",$idxf;
  # source
  $idxf = &add_record($root.'/source',$tic,$mut160,$label,$mhash,$ext);
  printf "source.idx: %s\n",$idxf;

  printf ".\n";
}


exit $?;

sub add_gitobject { # "objects/ff/..."
  my ($rootdir,$tic,$key,$label,$mhash,$ext) = @_;
  my $key16 = unpack'H*',$key;
  my $shard = substr$key16,0,2;
  my $indexn = substr($key16,2,5);

  my $qm = 'z'. &encode_base58($mhash);
  my $sdir = sprintf '%s/%s',$rootdir,$shard;
  mkdir $sdir unless -d $sdir;
  my $file = sprintf '%s/%s.%s',$sdir,$indexn,$ext;
  printf "record: %s: %u,%s,%s\n",$key16,$tic,$label,$qm;
  if (-e $file) { 
  open F,'+<',$file or warn $!;
  local $/ = "\n";
  my $header = <F>;
  while (<F>) { # search for equivalent record
     chomp;
    next if /^#/;
     my ($key,$rec) = split(': ',$_);
     if ($key eq $key16) {
        my (undef,$label,$qmi) = split(',',$rec);
        if ($qm eq $qmi) {
           printf "info: record for %s already present (%s*)\n",$label,substr($qmi,0,9);
           return $file;
        }
     }
  }
  tell(F,0,2);
  } else {
    open F,'>',$file or warn $!;
    printf F "--- # index for %s%s*\n",$shard,$indexn;
  }
  printf F "%s: %u,%s,%s\n",$key16,$tic,$label,$qm;
  close F;
  return $file;
}

sub add_record {
  my ($rootdir,$tic,$key,$label,$mhash,$ext) = @_;
  my $key32 = &encode_base32($key);
  my $key72 = &encode_basen($key,72); # alphanum and -+.@$%_,~ 

  my $shard = substr($key32,-3,2);
  my $indexn = substr($key32,0,8);

  my $qm = 'z'. &encode_base58($mhash);
  my $sdir = sprintf '%s/%s',$rootdir,$shard;
  mkdir $sdir unless -d $sdir;
  my $file = sprintf '%s/%s.%s',$sdir,$indexn,$ext;

  printf "record: %s: %u,%s,%s\n",$key72,$tic,$label,$qm;
  if (-e $file) { 
  open F,'+<',$file or warn $!;
  local $/ = "\n";
  my $header = <F>;
  while (<F>) { # search for equivalent record
     chomp;
    next if /^#/;
     my ($key,$rec) = split(': ',$_);
     if ($key eq $key72) {
        my (undef,$label,$qmi) = split(',',$rec);
        if ($qm eq $qmi) {
           printf "info: record for %s already present (%s)\n",$label,substr($qmi,0,9);
           return $file;
        }
     }
  }
  tell(F,0,2);
  } else {
    open F,'>',$file or warn $!;
    printf F "--- # index for %s*\n",$indexn;
  }
  printf F "%s: %u,%s,%s\n",$key72,$tic,$label,$qm;
  close F;
  return $file;
}

# -----------------------------------------------------
sub wikipage {
  my $sha1 = &khash('SHA1','wiki:',@_);
  my $key = 'z'.&encode_base58("$sha1");
  return $key;
}
# -----------------------------------------------------
sub khash { # keyed hash
   use Crypt::Digest qw();
   my $alg = shift;
   my $data = join'',@_;
   my $msg = Crypt::Digest->new($alg) or die $!;
      $msg->add($data);
   my $hash = $msg->digest();
   return $hash;
}
# -----------------------------------------------------
sub get_khash {
   use Crypt::Digest qw();
   my $alg = shift;
   my $file = pop;
   my $data = join'',@_;
  local *F; open F,$file or do { warn qq{"$file": $!}; return undef };
  binmode F unless $file =~ m/\.txt/;
  my $msg = Crypt::Digest::new($alg);
     $msg->add($data);
     $msg->addfile(*F);
   my $hash = $msg->digest();
   return $hash;
}
# -----------------------------------------------------
sub get_source {
 my $file = $_[0];
 my $source = $_[0];
   $source =~ s,$ENV{HOME},,;
 if (! -e $file) { return $source; } # default source

 local *F; open F,'<',$_[0]; local $/ = "\n";
 while (<F>) {
    if (m/(?<![\\\$])\$(Source):\s*([^\\\$]*?)\s*(?>!\\)?\$(?=['"\s])/) { # !~ on /\$.*$/
       $source = $2;
    }
 }
 close F;
 return $source;
}
# -----------------------------------------------------
sub encode_base32 {
  use MIME::Base32 qw();
  my $mh32 = uc MIME::Base32::encode($_[0]);
  return $mh32;
}
# -----------------------------------------------------
sub encode_base58 { # btc
  use Math::BigInt;
  use Encode::Base58::BigInt qw();
  my $bin = join'',@_;
  my $bint = Math::BigInt->from_bytes($bin);
  my $h58 = Encode::Base58::BigInt::encode_base58($bint);
  $h58 =~ tr/a-km-zA-HJ-NP-Z/A-HJ-NP-Za-km-z/;
  return $h58;
}
# -----------------------------------------------------
sub decode_base58 {
  use Math::BigInt;
  use Encode::Base58::BigInt qw();
  my $s = $_[0];
  # $e58 =~ tr/a-km-zA-HJ-NP-Z/A-HJ-NP-Za-km-z/;
  $s =~ tr/A-HJ-NP-Za-km-zIO0l/a-km-zA-HJ-NP-ZiooL/;
  my $bint = Encode::Base58::BigInt::decode_base58($s);
  my $bin = Math::BigInt->new($bint)->as_bytes();
  return $bin;
}
# -----------------------------------------------------
sub encode_basen { # n < 94;
  use Math::BigInt;
  my ($data,$radix) = @_;
  my $alphab = &alphab($radix);;
  my $mod = Math::BigInt->new($radix);
  #printf "mod: %s, lastc: %s\n",$mod,substr($alphab,$mod,1);
  my $h = '0x'.unpack('H*',$data);
  my $n = Math::BigInt->from_hex($h);
  my $e = '';
  while ($n->bcmp(0) == +1)  {
    my $c = Math::BigInt->new();
    ($n,$c) = $n->bdiv($mod);
    $e .= substr($alphab,$c->numify,1);
  }
  return scalar reverse $e;
}
# -----------------------------------------------------
sub alphab {
  my $radix = shift;
  my $alphab;
  if ($radix < 12) {
    $alphab = '0123456789-';
  } elsif ($radix == 12) {
    $alphab = '0123456789XE'; # tek, elk
  } elsif ($radix <= 16) {
    $alphab = '0123456789ABCDEF';
  } elsif ($radix <= 26) {
    $alphab = 'ABCDEFGHiJKLMNoPQRSTUVWXYZ';
  } elsif ($radix == 32) {
    $alphab = '0123456789ABCDEFGHiJKLMNoPQRSTUV'; # Triacontakaidecimal
    $alphab = join('',('A' .. 'Z', '2' .. '7')); # RFC 4648
    $alphab = '0123456789ABCDEFGHJKMNPQRSTVWXYZ'; # Crockfordś ![ILOU] (U:accidental obscenity)
    $alphab = 'ybndrfg8ejkmcpqxotluwisza345h769';  # z-base32 ![0lv2]

  } elsif ($radix == 36) {
    $alphab = 'ABCDEFGHiJKLMNoPQRSTUVWXYZ0123456789';
  } elsif ($radix <= 38) {
    $alphab = '0123456789ABCDEFGHiJKLMNoPQRSTUVWXYZ.-';
  } elsif ($radix <= 40) {
    $alphab = 'ABCDEFGHiJKLMNoPQRSTUVWXYZ0123456789-_.+';
  } elsif ($radix <= 43) {
    $alphab = 'ABCDEFGHiJKLMNoPQRSTUVWXYZ0123456789 -+.$%*';
  } elsif ($radix == 58) {
    $alphab = '123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz';
  } elsif ($radix == 62) {
    $alphab = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
  } elsif ($radix <= 81) {
    #                   1         2         3         4         5         6         7         8
    #          123456789012345678901234567890123456789012345678901234567890123456789012345678901
    $alphab = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-+.@$%_,~*:#!?^=;/`';
  } else { # n < 94
    $alphab = '-0123456789'. 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.
                             'abcdefghijklmnopqrstuwvxyz'.
             q/+.@$%_,~`'=;!^[]{}()#&/.      '<>:"/\\|?*'; #
  }
  # printf "// alphabet: %s (%uc)\n",$alphab,length($alphab);
  return $alphab;
}
# -----------------------------------------------------
sub fname { # extract filename etc...
  my $f = shift;
  $f =~ s,\\,/,g; # *nix style !
  my $s = rindex($f,'/');
  my $fpath = '.';
  if ($s > 0) {
    $fpath = substr($f,0,$s);
  } else {
    use Cwd;
    $fpath = Cwd::getcwd();
  }
  my $file = substr($f,$s+1);
  if (-d $f) {
    return ($fpath,$file);
  } else {
  my $bname;
  my $p = rindex($file,'.');
  my $ext = lc substr($file,$p+1);
     $ext =~ s/\~$//;
  if ($p > 0) {
    $bname = substr($file,0,$p);
    $ext = lc substr($file,$p+1);
    $ext =~ s/\~$//;
  } else {
    $bname = $file;
    $ext = &get_ext($f);
  }
  $bname =~ s/\s+\(\d+\)$//; # remove (1) in names ...

  return ($fpath,$file,$bname,$ext);

  }
}
# -----------------------------------------------------
sub get_ext {
  my $file = shift;
  my $ext = $1 if ($file =~ m/\.([^\.]+)/);
  if (! $ext) {
    my %ext = (
    text => 'txt',
    'application/octet-stream' => 'blob',
    'application/x-perl' => 'pl'
    );
    my $type = &get_type($file);
    if (exists $ext{$type}) {
       $ext = $ext{$type};
    } else {
      $ext = ($type =~ m'/(?:x-)?(\w+)') ? $1 : 'ukn';
    }
  }
  return $ext;
}
sub get_type { # to be expended with some AI and magic ...
  my $file = shift;
  use File::Type;
  my $ft = File::Type->new();
  my $type = $ft->checktype_filename($file);
  if ($type eq 'application/octet-stream') {
    my $p = rindex $file,'.';
    if ($p>0) {
     $type = 'files/'.substr($file,$p+1); # use the extension
    }
  }
  return $type;
}
# -----------------------------------------------------
1; # $Source: /my/perl/scripts/index.pl$
