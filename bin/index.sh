#

# usage:
# sh index.sh file
#
export PATH=../bin:$PATH
file="$1"
algo=SHA1
fname=${file##*/}
bname=${fname%.*}
qm=$(echo -n $bname | perl -S hashr.pl -q $algo)
echo $bname: $qm
